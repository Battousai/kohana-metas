<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Metas_Core
{

    /**
     * The meta tags
     * @var array
     */
    protected $_metas = array();

    /**
     * Helper method for chaining only
     * @return Metas Metas instance
     */
    public static function factory()
    {
        return new Metas;
    }

    /**
     * Adicionar nova meta
     *
     * @example
     *     $metas->addMeta(array(
     *          'rel'     =>'alternate',
     *          'hreflang'=>'es',
     *          'href'    =>'https://www.ordemdospsicologos.pt/es'
     *     ));
     *
     * @param array $attributes Outros atributos
     *
     * @return Metas Instância de Metas
     */
    public function addMeta(array $attributes)
    {
        $this->addRaw('meta', $attributes);
        return $this;
    }

    /**
     * Suporte para tags do género:
     *
     * <link rel="alternate" hreflang="x-default" href="https://www.ordemdospsicologos.pt/en">
     *
     * @param string $tag       Tag
     * @param array $attributes Outros atributos
     *
     * @return Metas Instância de Metas
     */
    public function addRaw($tag, array $attributes = NULL)
    {
        $this->_metas[] = new Node($tag, $attributes);

        return $this;
    }

    /**
     * Reinicia o container
     * @return Metas Instância de Metas
     */
    public function clear()
    {
        $this->_metas = array();

        return $this;
    }

    public function render()
    {
        $compiled = '';
        foreach ($this->_metas as $node)
        {
            /* @var $node Node */
            $compiled .= '<'.$node->tag().HTML::attributes($node->attributes()).'>'."\n";
        }

        return $compiled;
    }

}