<?php

class Metas_Node
{
    /**
     * @var string Tag name
     */
    protected $_tag;
    /**
     *
     * @var array Node attributes
     */
    protected $_attributes;

    public function __construct($tag, array $attributes)
    {
        $this->tag($tag);
        $this->attributes(self::_formatAttributes($attributes));
    }

    public function tag($tag = NULL)
    {
        if ($tag === NULL)
            return $this->_tag;

        $this->_tag = $tag;
    }

    public function attributes(array $attributes = NULL)
    {
        if (empty($attributes))
            return $this->_attributes;

        $this->_attributes = $attributes;
    }

    protected static function _formatAttributes(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $attributes[$key] = self::_formatAttribute($value);
        }

        return $attributes;
    }

    protected static function _formatAttribute($attribute)
    {
        return Text::limit_chars(strip_tags(preg_replace(
                '~\s+~',
                ' ',
                $attribute
            )), 200, '...', TRUE);
    }
}